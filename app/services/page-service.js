import Ember from 'ember';

export default Ember.Service.extend({
	pages :  [ { 
			name : "home",
			title : "Home",
			route : "index"
		},
		{
			name : "about",
			title : "About",
			route : "about"
		} ],
});
